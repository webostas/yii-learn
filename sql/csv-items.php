<?php

ini_set("auto_detect_line_endings", true);

// Число = От 1 до числа
// Массив - от, до
$config = [
    'articul' => 'ART',
    'types' => 5, // max types
    'material' => 5,
    'tematic' => 5,
    'description' => 'Suspendisse tempus sodales laoreet. Cras vestibulum, felis et aliquam luctus, lacus dui aliquam tortor, nec molestie lacus neque ut turpis. Nam eget ex sit amet felis tempor scelerisque. Morbi blandit urna vitae dictum scelerisque. Proin eleifend tincidunt mi, et malesuada elit bibendum quis. Nullam mollis magna eros. Nunc id tortor in elit sagittis consectetur vel quis mi.',
    'weight' => [10, 100],
    'price' => [10, 150],
    'image' => ['images/', 1, 5, '.png']
];

// CSV File
$fp = fopen('items.csv', 'w');
// Headers
$string = ["articul", "title", "description", "material_id", "type_id", "tematic_id", "weight", "price", "image"];
fputcsv($fp, $string);


for ($i = 1; $i < 100000; $i++) {
    $articul = $config['articul'] . (time() + $i);
    $material = rand(1, $config['material']);
    $title = "Item";
    $type = rand(1, $config['types']);
    $tematic = rand(1, 5);
    $price = rand($config['price'][0], $config['price'][1]) * 100;
    $description = $config['description'];
    $insertion = rand(1, 3);
    $weight = rand($config['weight'][0], $config['weight'][1]);
    $image = $config['image'][0] . rand($config['image'][1], $config['image'][2]) . $config['image'][3];
    $string = [$articul, $title, $description, $material, $type, $tematic, $weight, $price, $image];

    fputcsv($fp, $string);
}

fclose($fp);