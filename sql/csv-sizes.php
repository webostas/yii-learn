<?php

ini_set("auto_detect_line_endings", true);

// Число = От 1 до числа
// Массив - от, до
$config = [
'size' => [15,20],
'amount' => 20,
];

// CSV File
$fp = fopen('sizes.csv', 'w');
// Headers
$string = ["item_id", "size", "amount"];

fputcsv($fp, $string);


for( $i=1; $i < 100000; $i++ )
{
  $item_id = $i;
  $amount = rand(1,$config['amount']);
  $size = rand($config['size'][0], $config['size'][1]);

  $string = [$item_id, $size, $amount];

  fputcsv($fp, $string);

  $amount = rand(1,$config['amount']);
  $size = rand($config['size'][0], $config['size'][1]);

  $string = [$item_id, $size, $amount];

  fputcsv($fp, $string);
}

fclose($fp);