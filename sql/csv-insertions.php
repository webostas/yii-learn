<?php

ini_set("auto_detect_line_endings", true);

// Число = От 1 до числа
// Массив - от, до
$config = [
'insertions' => 7,
'colors' => ["red", "green", "blue", "yellow", "white"],
'forms' => 2,
'amount' => [1,10],
'weight' => [1,5]
];

// CSV File
$fp = fopen('insertions.csv', 'w');
// Headers
$string = ["item_id", "insertion_id", "color", "amount","form","weight"];

fputcsv($fp, $string);


for( $i=1; $i < 100000; $i++ )
{
  $item_id = $i;
  $insertion_id = rand(1, $config['insertions']);
  $color = $config['colors'][rand(1,count($config['colors'])-1)];
  $amount = rand($config['amount'][0], $config['amount'][1]);
  $form = rand(1, $config['forms'][1]);
  $weight = rand($config['weight'][0], $config['weight'][1]);

  $string = [$item_id, $insertion_id, $color, $amount, $form, $weight];

  fputcsv($fp, $string);

  $insertion_id = rand(1, $config['insertions']);
  $color = $config['colors'][rand(1,count($config['colors'])-1)];
  $amount = rand($config['amount'][0], $config['amount'][1]);
  $form = rand($config['forms'][0], $config['forms'][1]);
  $weight = rand($config['weight'][0], $config['weight'][1]);

  $string = [$item_id, $insertion_id, $color, $amount, $form, $weight];

  fputcsv($fp, $string);
}

fclose($fp);