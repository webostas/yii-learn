<?php

namespace app\models;

use Yii;
use yii\base\Model;

class CreateForm extends Model
{

    public $title;
    public $price;
    public $description;
    public $material_id;
    public $type_id;
    public $tematic_id;
    public $weight;
    public $insertion;
    public $insertions;
    
    public function rules()
    {
        return [
            [['title', 'price', 'description', 'weight', 'insertions'], 'required', 'message' => 'Это поле не может быть пустым'],
            [['price', 'weight'], 'number', 'message' => 'Это не число'],
            ['weight', 'compare', 'compareValue' => 0, 'operator' => '>', 'message' => 'Вес больше 0'],
            ['price', 'compare', 'compareValue' => 0, 'operator' => '>', 'message' => 'Цена больше 0'],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'title' => 'Название изделия',
            'price' => 'Цена',
            'description' => 'Описание',
            'material_id' => 'Материал',
            'type_id' => 'Вид',
            'tematic_id' => 'Тематика',
            'insertions' => 'Вставки',
            'weight' => 'Средний вес',
        ];
    }
}