<?php

namespace app\models;

use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "item".
 *
 * @property integer $id
 * @property string $articul
 * @property string $title
 * @property string $description
 * @property integer $material_id
 * @property integer $type_id
 * @property integer $tematic_id
 * @property integer $weight
 * @property integer $price
 * @property string $image
 *
 * @property Material $material
 * @property Type $type
 * @property Tematic $tematic
 * @property ItemInsertion[] $itemInsertions
 * @property ItemPhoto[] $itemPhotos
 * @property ItemSize[] $itemSizes
 */
class Item extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['articul', 'title', 'description', 'tematic_id', 'image'], 'required'],
            [['title', 'description', 'image'], 'string'],
            [['material_id', 'type_id', 'tematic_id', 'weight', 'price'], 'integer'],
            [['articul'], 'string', 'max' => 50],
            [['articul'], 'unique'],
            [['material_id'], 'exist', 'skipOnError' => true, 'targetClass' => Material::className(), 'targetAttribute' => ['material_id' => 'id']],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => Type::className(), 'targetAttribute' => ['type_id' => 'id']],
            [['tematic_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tematic::className(), 'targetAttribute' => ['tematic_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'articul' => 'Articul',
            'title' => 'Title',
            'description' => 'Description',
            'material_id' => 'Material ID',
            'type_id' => 'Type ID',
            'tematic_id' => 'Tematic ID',
            'weight' => 'Weight',
            'price' => 'Price',
            'image' => 'Image',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMaterial()
    {
        return $this->hasOne(Material::className(), ['id' => 'material_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(Type::className(), ['id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTematic()
    {
        return $this->hasOne(Tematic::className(), ['id' => 'tematic_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemInsertions()
    {
        return $this->hasMany(ItemInsertion::className(), ['item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemPhotos()
    {
        return $this->hasMany(ItemPhoto::className(), ['item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemSizes()
    {
        return $this->hasMany(ItemSize::className(), ['item_id' => 'id']);
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            foreach ($this->itemInsertions as $model)
                $model->delete();
            foreach ($this->itemSizes as $model)
                $model->delete();
            return true;
        } else {
            return false;
        }
    }

    public static function find()
    {
        return new ItemQuery(get_called_class());
    }
}

// Query
class ItemQuery extends ActiveQuery
{

    public function withAssertion($id)
    {
        return $this->joinWith('itemInsertions', 'item_insertion.item_id=item.id')->where(['item_insertion.insertion_id' => $id])->distinct();

    }

    public function withMin($min)
    {
        return $this->andWhere(['>=', 'item.weight', $min]);
    }

    public function withMax($max)
    {
        return $this->andWhere(['<=', 'item.weight', $max]);
    }    
}