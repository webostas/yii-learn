<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tematic".
 *
 * @property integer $id
 * @property string $title
 *
 * @property Item[] $items
 */
class Tematic extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tematic';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems()
    {
        return $this->hasMany(Item::className(), ['tematic_id' => 'id'])->inverseOf('tematic');
    }
}
