<?php

namespace app\models;

use Yii;
use yii\base\Model;

class EditForm extends Model
{
    public $title;
    public $price;
    public $description;
    public $material_id;
    public $type_id;
    public $tematic_id;
    public $weight;
    public $insertion;
    public $insertions;
    public $id;
    public $articul;

    public function rules()
    {
        return [
            [['title', 'price', 'description', 'weight', 'insertions', 'id', 'articul'], 'required'],
            [['price', 'weight'], 'number'],
        ];
    }    

    public function attributeLabels()
    {
        return [
            'title' => 'Название изделия',
            'price' => 'Цена',
            'description' => 'Описание',
            'material_id' => 'Материал',
            'type_id' => 'Вид',
            'tematic_id' => 'Тематика',
            'insertions' => 'Вставки',
            'weight' => 'Средний вес',
        ];
    }
}