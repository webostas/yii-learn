<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "item_insertion".
 *
 * @property integer $id
 * @property integer $item_id
 * @property integer $insertion_id
 * @property string $color
 * @property integer $amount
 * @property integer $form
 * @property integer $weight
 *
 * @property Item $item
 * @property Insertion $insertion
 */
class ItemInsertion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'item_insertion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_id', 'insertion_id', 'color', 'amount', 'form', 'weight'], 'required'],
            [['item_id', 'insertion_id', 'amount', 'form', 'weight'], 'integer'],
            [['color'], 'string', 'max' => 11],
            [['item_id'], 'exist', 'skipOnError' => true, 'targetClass' => Item::className(), 'targetAttribute' => ['item_id' => 'id']],
            [['insertion_id'], 'exist', 'skipOnError' => true, 'targetClass' => Insertion::className(), 'targetAttribute' => ['insertion_id' => 'id']],
            ['amount', 'compare', 'compareValue' => 1, 'operator' => '>='],
            ['weight', 'compare', 'compareValue' => 1, 'operator' => '>='],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'item_id' => 'Item ID',
            'insertion_id' => 'Insertion ID',
            'color' => 'Color',
            'amount' => 'Amount',
            'form' => 'Form',
            'weight' => 'Weight',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(Item::className(), ['id' => 'item_id'])->inverseOf('itemInsertions');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInsertion()
    {
        return $this->hasOne(Insertion::className(), ['id' => 'insertion_id'])->inverseOf('itemInsertions');
    }
}
