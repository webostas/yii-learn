<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "insertion".
 *
 * @property integer $id
 * @property string $title
 *
 * @property ItemInsertion[] $itemInsertions
 */
class Insertion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'insertion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemInsertions()
    {
        return $this->hasMany(ItemInsertion::className(), ['insertion_id' => 'id'])->inverseOf('insertion');
    }

}
