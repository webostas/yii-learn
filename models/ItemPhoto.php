<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "item_photo".
 *
 * @property integer $id
 * @property integer $item_id
 * @property string $photo
 * @property string $photo_maneken
 * @property string $photo_model
 *
 * @property Item $item
 */
class ItemPhoto extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'item_photo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_id'], 'required'],
            [['item_id'], 'integer'],
            [['photo', 'photo_maneken', 'photo_model'], 'string', 'max' => 255],
            [['item_id'], 'exist', 'skipOnError' => true, 'targetClass' => Item::className(), 'targetAttribute' => ['item_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'item_id' => 'Item ID',
            'photo' => 'Photo',
            'photo_maneken' => 'Photo Maneken',
            'photo_model' => 'Photo Model',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(Item::className(), ['id' => 'item_id']);
    }
}
