<?php

namespace app\controllers;

use app\models\CreateForm;
use app\models\EditForm;
use app\models\Item;
use app\models\ItemInsertion;
use app\models\Type;
use app\models\Tematic;
use app\models\Material;
use app\models\Insertion;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

class StoreController extends \yii\web\Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionError()
    {
        $exception = \Yii::$app->errorHandler->exception;
        if ($exception !== null) {
            return $this->render('error', ['exception' => $exception]);
        }
    }

    public function actionIndex()
    {
        $params = \Yii::$app->request->get();
        $query = Item::find();

        // Query
        if (!empty($params["insertion"])) {
            $query->withAssertion($params['insertion']);
        }
        if (!empty($params["min"])) {
            $query->withMin($params['min']);
        }
        if (!empty($params["max"])) {
            $query->withMin($params['max']);
        }


        $count = $query->count('id');

        $pagination = new Pagination([
            'defaultPageSize' => 8,
            'totalCount' => $count,
        ]);

        $items = $query
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        $insertions = Insertion::find()->all();

        return $this->render('index', [
            'items' => $items,
            'pagination' => $pagination,
            'insertions' => $insertions,
        ]);
    }

    public function actionView()
    {
        $request = \Yii::$app->request;

        $art = $request->get('id');

        $item = Item::findOne(['id' => $art]);

        $sizes = ArrayHelper::map($item->itemSizes, 'size', 'amount');

        if ($art != null) {
            return $this->renderAjax('view', [
                'get' => $art,
                'item' => $item,
                'sizes' => $sizes,
            ]);
        }
    }

    public function actionNew()
    {
        $model = new CreateForm();

        $materials = ArrayHelper::map(Material::find()->all(), 'id', 'title');
        $types = ArrayHelper::map(Type::find()->all(), 'id', 'title');
        $tematic = ArrayHelper::map(Tematic::find()->all(), 'id', 'title');

        $insertions = ArrayHelper::map(Insertion::find()->all(), 'id', 'title');

        return $this->render('new',
            [
                'model' => $model,
                'materials' => $materials,
                'types' => $types,
                'tematic' => $tematic,
                'insertions' => $insertions,
            ]);
    }

    public function actionEdit()
    {

        $request = \Yii::$app->request;
        $id = $request->get('id');
        $item = Item::findOne($id);

        if (!$item) {
            throw new NotFoundHttpException('Товар не найден');
        }

        $model = new EditForm();

        // Form
        $insertions = ArrayHelper::map(Insertion::find()->all(), 'id', 'title');
        $materials = ArrayHelper::map(Material::find()->all(), 'id', 'title');
        $types = ArrayHelper::map(Type::find()->all(), 'id', 'title');
        $tematic = ArrayHelper::map(Tematic::find()->all(), 'id', 'title');

        $item_insertions = ItemInsertion::find()
            ->asArray()
            ->where(['item_id' => $id])
            ->all();

        return $this->render('edit',
            [
                'model' => $model,
                'materials' => $materials,
                'types' => $types,
                'tematic' => $tematic,
                'insertions' => $insertions,
                'item' => $item,
                'item_insertions' => $item_insertions,
            ]);
    }
}