<?php

namespace app\controllers;

use app\models\Item;
use app\models\ItemInsertion;
use yii\web\HttpException;

class DataController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionCreate()
    {
        $request = \Yii::$app->request;

        if ($request->isPost) {

            $params = $request->getBodyParam('CreateForm');

            $model = new Item();
            $model->attributes = $params;
            $model->image = "images/1.png";
            $model->articul = "ART" . (time()+100000);

            if ($model->validate()) {
                if (!($model->save())) {
                    throw new HttpException(500, 'Database Error');
                }
            } else {
                throw new HttpException(500, 'Validation Error');
            }

            $insertions = json_decode($params['insertion']);

            if (count($insertions) > 0) {
                foreach ($insertions as $insertion) {
                    $ins = new ItemInsertion();
                    $ins->item_id = $model->id;
                    $ins->insertion_id = $insertion[0];
                    $ins->color = $insertion[2];
                    $ins->amount = $insertion[3];
                    $ins->weight = $insertion[4];
                    $ins->form = 1;

                    if ($ins->validate()) {
                        if (!($ins->save())) {
                            throw new HttpException(500, 'Database Error');
                        }
                    }
                }
            }
            return $this->goHome();
        }
    }


    public function actionDelete()
    {
        $request = \Yii::$app->request;

        $id = $request->get('id');

        $item = Item::findOne($id);

        if ($item->delete()) {
            return $this->goHome();
        } else {
            throw new HttpException(500, 'Deleting Error');
        }
    }

    public function actionEdit()
    {
        $request = \Yii::$app->request;
        if ($request->isPost) {
            $params = $request->getBodyParam('EditForm');
            $model = Item::findOne($params['id']);
            $model->attributes = $params;

            if ($model->validate()) {

                if ($model->save()) {
                    return $this->goHome();
                } else {
                    throw new HttpException(500, 'Database Error');
                }
            } else {
                throw new HttpException(400, 'Validation Error');
            }
        }
        return $this->goHome();
    }

    public function actionAjaxcreate()
    {

        $request = \Yii::$app->request;
        $insertion = json_decode($request->post('params'));
        $id = $request->post('id');

        $model = new ItemInsertion();

        // Ajax
        $model->insertion_id = (int)$insertion[0];
        $model->form = (int)$insertion[1];
        $model->color = $insertion[2];
        $model->amount = (int)$insertion[3];
        $model->weight = (int)$insertion[4];

        $model->item_id = $id;
        
        if ($model->validate()) {
            if ($model->save()) {
                return $model->id;
            } else {
                return json_encode($model->getErrors()) . " " . $insertion[2];
            }
        } else {
            throw new HttpException(500);
        }
    }

    public function actionAjaxdelete()
    {
        $request = \Yii::$app->request;
        $insertion = $request->post('id');

        $model = ItemInsertion::findOne($insertion);

        if ($model->delete()) {
            return $model->id . " deleted";
        } else {
            throw new HttpException(500);
        }
    }
}