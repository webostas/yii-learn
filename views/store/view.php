<?php

use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\helpers\ArrayHelper;

?>

<div class="container-fluid">
    <div class="row">
        <h1><?= $item->title ?></h1>
        <div class="col-lg-6">
            <img src="<?= $item->image ?>" alt="" width="100%">
        </div>
        <div class="col-lg-6">
            <p><?= $item->description ?></p>
            <table class="table">
                <tr>
                    <td>Цена</td>
                    <td><?= $item->price ?> &#8381;</td>
                </tr>
                <tr>
                    <td>Цена за грамм</td>
                    <td><?php if ($item->weight > 0) echo round($item->price / $item->weight) ?></td>
                </tr>
                <tr>
                    <td>Артикул</td>
                    <td><?= $item->articul ?></td>
                </tr>
                <tr>
                    <td>Вид</td>
                    <td><?= $item->type->title ?></td>
                </tr>
                <tr>
                    <td>Тематика</td>
                    <td><?= $item->tematic->title ?></td>
                </tr>
                <tr>
                    <td>Вставки</td>
                    <td>
                        <?php
                        if (count($item->itemInsertions) > 0) {

                            foreach ($item->itemInsertions as $insertion) {
                                $svg = '<svg width="32" height="32"><rect width="32" height="32" style="fill:' . $insertion->color . ';stroke-width:1;stroke:rgb(0,0,0);" /></svg>';
                                echo $insertion->insertion->title . ": " . $insertion->amount . " шт. по " . $insertion->weight . " гр. Цвет: " . $svg . " <br>";
                            }
                        } else {
                            echo "Без вставок";
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>Размеры</td>
                    <td>
                        <?php
                        if ($sizes)
                            foreach ($sizes as $size => $amount)
                                print "<strong>$size</strong> ($amount шт)<br>";
                        else
                            print "Нет в наличии";
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>Материал</td>
                    <td><?= $item->material->title ?></td>
                </tr>
                <tr>
                    <td>Вес</td>
                    <td><?= $item->weight ?> гр</td>
                </tr>
            </table>
        </div>
    </div>
</div>