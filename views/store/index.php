<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\View;

$this->title = 'Каталог';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="row">

    <div class="col-lg-2">

        <h3>Вставки</h3>
        <p>Всего: <?= $pagination->totalCount ?></p>

        <ul class="nav">
            <li><a href="<?= Url::toRoute('store/index') ?>">Все</a></li>

            <?php

            foreach ($insertions as $ins) {
                echo '<li class="insertion"><a href="' . Url::toRoute(['store/index', 'insertion' => $ins->id]) . '">' . $ins->title . '</a></li>';
            }

            ?>
        </ul>

        <h3>Вес</h3>

        <form action="<?= Url::toRoute('store/index') ?>">

            <div class="form-group">
                <input type="number" name="min" class="form-control" placeholder="От"
                       value="<?= Yii::$app->request->get('min') ?>">
            </div>
            <div class="form-group">
                <input type="number" name="max" class="form-control" placeholder="До"
                       value="<?= Yii::$app->request->get('max') ?>">
            </div>

            <input type="hidden" name="insertion" value="<?= Yii::$app->request->get('insertion') ?>">

            <input type="submit" class="btn" value="Применить">
        </form>

    </div>
    <div class="col-lg-10">
        <?php foreach ($items as $item): ?>
            <div class="col-sm-3 col-md-3 col-lg-3">
                <div class="thumbnail">
                    <img src="<?= Html::encode("{$item->image}") ?>" alt="...">
                    <div class="caption">
                        <h3><?= Html::encode("{$item->title}") ?></h3>
                        <p><?= Html::encode("{$item->articul}") ?></p>
                        <p>Тематика: <?= Html::encode("{$item->tematic->title}") ?></p>
                        <a type="button " class="btn btn-primary btn-block" data-toggle="modal" data-target="#info"
                           id="modal-btn" rel="<?= $item->id ?>">
                            Купить
                            за <?= Html::encode("{$item->price}") ?> &#8381;
                        </a
                        <br><br>
                        <a class="btn btn-default" href="<?= Url::toRoute(['store/edit', 'id' => $item->id]) ?>"><span
                                class="glyphicon glyphicon-pencil   " aria-hidden="true"></span></a>
                        <a href="<?= Url::toRoute(['data/delete', 'id' => $item->id]) ?>" class="btn btn-default"
                           type="submit"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>

                    </div>
                </div>
            </div>
        <?php endforeach; ?>
        <?= LinkPager::widget(['pagination' => $pagination]) ?>
    </div>
</div>

<div class="modal fade" id="info" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Инфо</h4>
            </div>
            <div class="modal-body" id="mod"></div>
        </div>
    </div>
</div>
<script>
    $(document).on('click', '#modal-btn', function () {
        var id = $(this).attr("rel");
        $.ajax({
            type: "GET",
            url: "<?= Url::toRoute('store/view') ?>",
            data: {'id': id},
            cache: false,
            success: function (data) {
                $("#mod").html(data).fadeIn(200);
            }
        });
    });
    $('li > a').each(function () {
        if ($(this).prop("href") === window.location.href) {
            $(this).parent().css("background-color", "#eee");
        }
    });
</script>