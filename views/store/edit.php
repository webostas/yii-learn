<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\web\View;

$this->title = 'Редактировать';
$this->params['breadcrumbs'][] = $this->title;

/* @var $this yii\web\View */
/* @var $model app\models\CreateForm */
/* @var $form ActiveForm */

$this->registerJs(
    "var item_insertions = " . json_encode($item_insertions) . ";", View::POS_BEGIN, 'my-options');

$model->setAttributes($item->attributes, false);

?>
<div class="row">

    <h1>Редактировать</h1>

    <div class="col-md-12">

        <?php $form = ActiveForm::begin([
            'method' => 'post',
            'action' => Url::toRoute('data/edit'),
            'options' => ['enctype' => 'multipart/form-data'],
        ]); ?>

        <?= $form->field($model, 'title') ?>
        <?= $form->field($model, 'price')->input('number') ?>
        <?= $form->field($model, 'weight') ?>
        <?= $form->field($model, 'description')->textarea() ?>
        <?= $form->field($model, 'material_id')->dropDownList($materials) ?>
        <?= $form->field($model, 'type_id')->dropDownList($types) ?>
        <?= $form->field($model, 'tematic_id')->dropDownList($tematic) ?>

        <div class="form-group">
            <label for="exampleInputPassword1">Используемые вставки</label>
            <table class="table" id="include">
                <tr>
                    <td></td>
                    <td>
                        <?= $form->field($model, 'insertions')->dropDownList($insertions)->label(false) ?>
                    </td>
                    <td>
                        <select class="form-control value-form">
                            <option value="1" selected>Шар</option>
                            <option value="2">Овал</option>
                        </select>
                    </td>
                    <td>
                        <select class="form-control value-color">
                            <option value="white" selected>Белый</option>
                            <option value="yellow">Желтый</option>
                            <option value="red">Красный</option>
                            <option value="blue">Синий</option>
                            <option value="green">Зеленый</option>
                        </select>
                    </td>
                    <td>
                        <input class="form-control value-amount" type="number" placeholder="Количество">
                    </td>
                    <td>
                        <input class="form-control value-weight" type="number" placeholder="Вес">
                    </td>
                    <td>
                        <button class="btn" type="button" id="addInclude">Добавить</button>
                    </td>
                </tr>
            </table>
        </div>

        <input type="text" id="insertion" hidden>

        <?= $form->field($model, 'insertion')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'id')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'articul')->hiddenInput()->label(false) ?>


        <div class="form-group">
            <?= Html::submitButton('Редактировать', ['class' => 'btn btn-primary']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
<script>

    var includes = [];
    var table = $('#include');

    function fillTable(items) {

        items.forEach(function (item, i, arr) {

            var field_material = $("#editform-insertions option").eq(item['insertion_id'] - 1).text();
            var form = $(".value-form option").eq(item['form']).text();
            var color = $(".value-color option[value='" + item['color'] + "']").text();

            var current = [item['id'], item['insertion_id'], item['insertion_id'], item['color'], item['amount'], item['weight']];
            includes.push(current);

            setInsertions();

            table.append('<tr><td class=".id">' + item['id'] + ' </td><td>' + field_material + '</td><td>' + form + '</td><td>' + color + '</td><td>' + item['amount'] + ' шт</td><td>' + item['weight'] + '</td><td><a class="btn btn-default remove"><span class="glyphicon glyphicon-trash"></span></a></td></tr>');
        });
    }

    function setInsertions() {
        $('#editform-insertion').val(JSON.stringify(includes));
    }

    fillTable(item_insertions);

    //    REMOVE ELEMENT

    table.on('click', '.remove', function () {

        var index = $(this).closest('tr').index();
        var id = $(this).parents('tr').find('td:first').text();
        var row = $(this).closest('tr');

        $.ajax({
            type: "POST",
            url: "<?= Url::toRoute('data/ajaxdelete') ?>",
            data: {id},
            cache: false,
            success: function (data) {

                row.remove();
            }
        });
    });

    //    ADD ELEMENT

    $('#addInclude').click(function () {

        var table = $('#include');

        var row = $(this).closest('tr');

        var inc = row.find('#editform-insertions').val();
        var incf = row.find('#editform-insertions   option:selected').text();
        // GET form and value
        var form = row.find('.value-form').val();
        var formf = row.find('.value-form option:selected').text();
        // GET color and value
        var color = row.find('.value-color').val();
        var colorf = row.find('.value-color option:selected').text();
        // amount
        var quantity = parseInt(row.find('.value-amount').val(), 10);
        // weight
        var weight = parseFloat(row.find('.value-weight').val());
        // ready
        var current = [inc, form, color, quantity, weight];

        $.ajax({
            type: "POST",
            url: "<?= Url::toRoute('data/ajaxcreate') ?>",
            data: {'params': JSON.stringify(current), id: <?= $model->id ?>},
            cache: false,
            success: function (data) {
                table.append('<tr><td>' + data + '</td><td class=".id">' + incf + ' </td><td>' + formf + '</td><td>' + colorf + '</td><td>' + quantity + ' шт</td><td>' + weight + '</td><td><a class="btn btn-default remove"><span class="glyphicon glyphicon-trash"></span></a></td></tr>');
            }
        });
    });
</script>