<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

use yii\helpers\ArrayHelper;

$this->title = 'Добавить';
$this->params['breadcrumbs'][] = $this->title;

/* @var $this yii\web\View */
/* @var $model app\models\CreateForm */
/* @var $form ActiveForm */
?>

<div class="row">
    <h1>Добавить</h1>
    <div class="col-md-12">

        <?php $form = ActiveForm::begin([
            'method' => 'post',
            'action' => Url::toRoute('data/create'),
            'options' => ['enctype' => 'multipart/form-data'],
        ]); ?>

        <?= $form->field($model, 'title') ?>
        <?= $form->field($model, 'price')->input('number') ?>
        <?= $form->field($model, 'weight')->input('number') ?>
        <?= $form->field($model, 'description')->textarea() ?>
        <?= $form->field($model, 'material_id')->dropDownList($materials) ?>
        <?= $form->field($model, 'type_id')->dropDownList($types) ?>
        <?= $form->field($model, 'tematic_id')->dropDownList($tematic) ?>
        <?= $form->field($model, 'insertion')->hiddenInput()->label(false) ?>

        <div class="form-group">
            <label for="exampleInputPassword1">Используемые вставки</label>
            <table class="table" id="include">
                <tr>
                    <td>
                        <?= $form->field($model, 'insertions')->dropDownList($insertions)->label(false) ?>
                    </td>
                    <td>
                        <select class="form-control value-form">
                            <option value="1" selected>Шар</option>
                            <option value="2">Овал</option>
                        </select>
                    </td>
                    <td>
                        <select class="form-control value-color">
                            <option value="white" selected>Белый</option>
                            <option value="yellow">Желтый</option>
                            <option value="red">Красный</option>
                            <option value="blue">Синий</option>
                            <option value="green">Зеленый</option>
                        </select>
                    </td>
                    <td>
                        <input class="form-control value-amount" type="number" placeholder="Количество">
                    </td>
                    <td>
                        <input class="form-control value-weight" type="number" placeholder="Вес">
                    </td>
                    <td>
                        <button class="btn" type="button" id="addInclude">Добавить</button>
                    </td>
                </tr>
            </table>
        </div>

        <input type="text" id="insertion" hidden>

        <div class="form-group">
            <?= Html::submitButton('Добавить', ['class' => 'btn btn-primary']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
<script>

    var includes = [];

    $("#include").on('click', '.remove', function () {

        var index = $(this).closest('tr').index();

        includes.splice(index - 1, 1);

        $('#createform-insertion').val(JSON.stringify(includes));

        $(this).closest('tr').remove();

    });


    $('#addInclude').click(function () {

        var row = $(this).closest('tr');

        var inc = row.find('#createform-insertions').val();
        var incf = row.find('#createform-insertions   option:selected').text();

        var form = row.find('.value-form').val();
        var formf = row.find('.value-form option:selected').text();

        var color = row.find('.value-color').val();
        var colorf = row.find('.value-color option:selected').text();

        var quantity = parseInt(row.find('.value-amount').val(), 10);
        var weight = parseFloat(row.find('.value-weight').val());
        var current = [inc, form, color, quantity, weight];

        if ((weight > 0) && (quantity > 0)) {
            includes.push(current);

            $('#createform-insertion').val(JSON.stringify(includes));

            var table = $('#include');

            row.find('.value-inc option:selected').prop('disabled', 'disabled');

            table.append('<tr><td>' + incf + '</td><td>' + formf + '</td><td>' + colorf + '</td><td>' + quantity + ' шт</td><td>' + weight + '</td><td><a class="btn btn-default remove"><span class="glyphicon glyphicon-trash"></span></a></td></tr>');

        }

    });
</script>