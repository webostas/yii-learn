# Yii-learn

Ювелирный интернет-магазин (ограниченный функционал) на Yii 2.x

### Web

Контроллеры:
- StoreController для взаимодействия
- DataController для работы с данными

Модели (автогенерация для каждой таблицы DB). MySQL, InnoDB для поддержки транзакций
- Item (товар)
- Item_Insertion (вставки)
- Insertion (материал вставки)
- Material (материал товара)
- Tematic
- Type
- Item_Size (размеры и остатки)
- Item_Photo (дополнительные фото)

Представления

- Index (главная страница - каталог)
- View (подробная информация)
- New (добавление нового изделия)
- Edit (редактирование)

### SQL

Генерирует записи для таблицы Items. Создает CSV файл
> csv-items.php

Генерирует записи для таблицы Items_Insertion.
> csv-insertions.php

Генерирует записи для таблицы Items_Size.
> csv-sizes.php

Дамп для импорта
> store.sql

В папке
> /sql/

Импорт через BULK INSERT для скорости

[HeidiSQL](http://www.heidisql.com/)

### Модель данных

![Модель данных](database.png)

(реализованная)

### Установка

```
$ php composer.phar install
```